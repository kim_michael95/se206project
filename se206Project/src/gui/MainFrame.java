package gui;

import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import gui.subPanels.AudioListPane;
import gui.subPanels.AudioPane;
import gui.subPanels.CommentaryPane;
import main.Main;
import managers.VideoManager;
import tools.MaxCharacterLimiter;
import tools.fileTools.AudioFile;
import tools.fileTools.FileOpener;

import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")

/**
 * This is the MainFrame gui that launches up when you select a video.
 * The purpose of this class is to bring together all the separate gui parts into one frame.
 * It uses the videoManager which manages all the video functionality.
 * @author jkpop
 *
 */
public class MainFrame extends JFrame {
	// Declaring useful variables
	private FileOpener fo = new FileOpener(".avi", this);
	private String vidPath;
	private JPanel contentPane;
	private boolean rw = false;
	private boolean ff = false;
	private VideoManager videoManager = new VideoManager();
	private ArrayList<AudioFile> audioArrayList = new ArrayList<AudioFile>();
	

	// Declaring gui components as class fields as they are needed in a class method
	private final JButton btnMute = new JButton();
	private final JSlider volSlider = new JSlider();
	private final JButton btnPlay = new JButton();
	private final JButton btnSkipBack = new JButton();
	private final JButton btnFF = new JButton();
	private final JButton btnRW = new JButton();
	private final JButton btnSkipForward = new JButton();
	private final JSlider vidProgress = new JSlider();
	private final JButton btnGo = new JButton();

	public MainFrame(final String path) throws Exception {
		// Setting up video path
		this.vidPath = path;
		videoManager.setVideoPath(vidPath);

		// Setting up the contentPane & JFrame
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(100, 100);
		setTitle("VidiVox Beta");
		setMinimumSize(new Dimension(1192, 500));
		setPreferredSize(new Dimension(1192, 500));

		// Component declarations
		final JPanel sideFeatures = new JPanel();
		JPanel sidePane = new JPanel();
		final JTabbedPane listPane = new JTabbedPane(JTabbedPane.TOP);
		JTabbedPane commentaryPane = new JTabbedPane(JTabbedPane.TOP);
		JTabbedPane audioPane = new JTabbedPane(JTabbedPane.TOP);
		final AudioListPane audioList = new AudioListPane(audioArrayList, vidPath);
		AudioPane audio = new AudioPane(videoManager, audioArrayList, audioList);
		CommentaryPane commentary = new CommentaryPane(videoManager, audioList, this);
		JPanel timePane = new JPanel();
		JPanel volumePane = new JPanel();
		JPanel playbackButtonPane = new JPanel();
		JPanel playbackPane = new JPanel();
		final JPanel buttonsPane = new JPanel();
		JPanel videoPane = new JPanel();
		final JPanel progressPane = new JPanel();
		final JLabel length = new JLabel();
		final JLabel currentTime = new JLabel();
		final JTextField timeField = new JTextField("00:00");
		
		// Setting up the time field
		timeField.setDocument(new MaxCharacterLimiter(5));
		timeField.setHorizontalAlignment(JTextField.CENTER);
		timeField.setPreferredSize(new Dimension(50, 23));
		timeField.setText("00:00");

		// Setting up media components
		videoManager.setSize(new Dimension(640, 360));

		// Setting up the nested panels
		videoPane.setLayout(new BorderLayout());
		buttonsPane.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		buttonsPane.setBorder(new EmptyBorder(0, 0, 10, 0));
		timePane.setLayout(new BorderLayout(5, 5));
		timePane.setBorder(BorderFactory.createTitledBorder("Go to:"));
		volumePane.setLayout(new FlowLayout());
		progressPane.setLayout(new BorderLayout(10, 0));
		progressPane.setBorder(new EmptyBorder(0, 10, 0, 10));
		playbackPane.setBackground(Color.BLACK);
		playbackPane.setLayout(new BorderLayout());
		sideFeatures.setLayout(new BorderLayout());
		sidePane.setLayout(new BorderLayout());

		// Setting up the video progress slider
		vidProgress.setFocusable(false);
		vidProgress.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (vidProgress.getValueIsAdjusting()) {
					videoManager.setTime(vidProgress.getValue());
				}
			}
		});

		// Setting up a MenuBar
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);
		ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);
		
		final JMenuBar menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("File");
		JMenu viewMenu = new JMenu("View");

		JMenuItem openFile = new JMenuItem("Open file...");
		JMenuItem closeFile = new JMenuItem("Close video");
		JMenuItem close = new JMenuItem("Exit program");

		JCheckBoxMenuItem showOptions = new JCheckBoxMenuItem("Show options");
		JCheckBoxMenuItem showList = new JCheckBoxMenuItem("Show audio list");
		JCheckBoxMenuItem showFFRW = new JCheckBoxMenuItem("Show fast forward and rewind");
		showOptions.setSelected(true);
		showList.setSelected(true);
		showFFRW.setSelected(true);

		fileMenu.add(openFile);
		fileMenu.add(closeFile);
		fileMenu.addSeparator();
		fileMenu.add(close);
		viewMenu.add(showOptions);
		viewMenu.add(showList);
		viewMenu.addSeparator();
		viewMenu.add(showFFRW);
		menuBar.add(fileMenu);
		menuBar.add(viewMenu);

		// Setting up openFile option
		openFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File vid = fo.openFile();
				if (vid != null) {
					vidPath = vid.getAbsolutePath();
					videoManager.changeVideo(vidPath);
					btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/pause.png")));
					audioList.clearList();
					enableControl();
				}
			}
		});

		// Setting up closeFile option
		closeFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoManager.closeVideo();
				btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/play.png")));
				length.setText("00:00");
				audioList.clearList();
				disableControlExclPlay(false);
			}
		});

		// Setting up exit option
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		// Setting up show options option
		showOptions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (sideFeatures.isVisible() == true) {
					sideFeatures.setVisible(false);
					if (listPane.isVisible() == false){
						setMinimumSize(new Dimension(600, 500));
						setPreferredSize(new Dimension(600, 500));
					} else {
						setMinimumSize(new Dimension(778, 500));
						setPreferredSize(new Dimension(778, 500));
					}
					pack();
				} else {
					sideFeatures.setVisible(true);
					if (listPane.isVisible() == false){
						setMinimumSize(new Dimension(1014, 500));
						setPreferredSize(new Dimension(1014, 500));
					} else {
						setMinimumSize(new Dimension(1192, 500));
						setPreferredSize(new Dimension(1192, 500));
					}
					pack();
				}
			}
		});
		
		// Setting up show audio list option
		showList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listPane.isVisible() == true) {
					listPane.setVisible(false);
					if (sideFeatures.isVisible() == false){
						setMinimumSize(new Dimension(600, 495));
						setPreferredSize(new Dimension(600, 495));
					} else {
						setMinimumSize(new Dimension(1014, 495));
						setPreferredSize(new Dimension(1014, 495));
					}
					pack();
				} else {
					listPane.setVisible(true);
					if (sideFeatures.isVisible() == false){
						setMinimumSize(new Dimension(778, 495));
						setPreferredSize(new Dimension(778, 495));
					} else {
						setMinimumSize(new Dimension(1192, 495));
						setPreferredSize(new Dimension(1192, 495));
					}
					pack();
				}
			}
		});

		// Setting up show FF RW option
		showFFRW.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnFF.isVisible() == true) {
					btnFF.setVisible(false);
					btnRW.setVisible(false);
				} else {
					btnFF.setVisible(true);
					btnRW.setVisible(true);
				}
			}
		});

		// Setting up the go to time button
		btnGo.setFocusPainted(false);
		btnGo.setPreferredSize(new Dimension(40, 20));
		btnGo.setIcon(new ImageIcon(Main.class.getResource("/images/go.png")));
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoManager.goTime(timeField.getText());
			}
		});

		// Setting up the play/pause/replay button
		btnPlay.setFocusPainted(false);
		btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/pause.png")));
		btnPlay.setPreferredSize(new Dimension(40, 40));
		btnPlay.setFocusPainted(false);
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				if (rw == true) {
					videoManager.rewind(false);
					btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/pause.png")));
					rw = false;
					enableControl();
				} else if (ff == true) {
					videoManager.fastForward(false);
					btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/pause.png")));
					ff = false;
					enableControl();
				} else if (videoManager.isPlaying() == true) {
					btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/play.png")));
					videoManager.pauseVideo(true);
				} else if (videoManager.getVisibility() == false) {
					btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/pause.png")));
					videoManager.setVisibility(true);
					videoManager.playVideo(path);
					enableControl();
				} else {
					btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/pause.png")));
					videoManager.pauseVideo(false);
				}
			}
		});

		// Setting up the Fast Forward button
		btnFF.setFocusPainted(false);
		btnFF.setIcon(new ImageIcon(Main.class.getResource("/images/ff.png")));
		btnFF.setPreferredSize(new Dimension(30, 30));
		btnFF.setFocusPainted(false);
		btnFF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disableControlExclPlay(true);
				videoManager.fastForward(true);
				btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/play.png")));
				ff = true;
			}
		});

		// Setting up the Rewind button
		btnRW.setFocusPainted(false);
		btnRW.setIcon(new ImageIcon(Main.class.getResource("/images/rw.png")));
		btnRW.setPreferredSize(new Dimension(30, 30));
		btnRW.setFocusPainted(false);
		btnRW.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disableControlExclPlay(true);
				videoManager.rewind(true);
				btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/play.png")));
				rw = true;
			}
		});

		// Setting up the skip back button
		btnSkipBack.setFocusPainted(false);
		btnSkipBack.setIcon(new ImageIcon(Main.class.getResource("/images/sb.png")));
		btnSkipBack.setPreferredSize(new Dimension(30, 30));
		btnSkipBack.setFocusPainted(false);
		btnSkipBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoManager.skipBack();
			}
		});

		// Setting up the skip forward button
		btnSkipForward.setFocusPainted(false);
		btnSkipForward.setIcon(new ImageIcon(Main.class.getResource("/images/sf.png")));
		btnSkipForward.setPreferredSize(new Dimension(30, 30));
		btnSkipForward.setFocusPainted(false);
		btnSkipForward.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoManager.skipForward();
			}
		});

		// Setting up the mute button
		btnMute.setPreferredSize(new Dimension(30, 30));
		btnMute.setIcon(new ImageIcon(Main.class.getResource("/images/unmuted.png")));
		btnMute.setFocusPainted(false);
		btnMute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoManager.muteToggle();
			}
		});

		// Setting up the volume slider
		volSlider.setValue(100);
		volSlider.setMaximum(100);
		volSlider.setMinimum(0);
		volSlider.setMinorTickSpacing(10);
		volSlider.setPaintTicks(true);
		volSlider.setSnapToTicks(true);
		volSlider.setFocusable(false);
		volSlider.setPreferredSize(new Dimension(100, 50));
		volSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (volSlider.getValueIsAdjusting()) {
					videoManager.setVolume(volSlider.getValue());
					videoManager.mute(false);
				}
			}
		});

		// Setting up video progress timer
		final Timer timer = new Timer(200, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				vidProgress.setValue((int) videoManager.getTime());
				currentTime.setText(videoManager.calculateTime((int) videoManager.getTime()));
				if (videoManager.isMute() == true || videoManager.getVolume() == 0) {
					btnMute.setIcon(new ImageIcon(Main.class.getResource("/images/muted.png")));
				} else {
					btnMute.setIcon(new ImageIcon(Main.class.getResource("/images/unmuted.png")));
				}
			}
		});
		timer.start();

		// Adding a video playing event listener
		videoManager.getComponent().getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			@Override
			public void playing(MediaPlayer mediaPlayer) {
				// Initializing progress bar and setting time stamps
				vidProgress.setMaximum((int) videoManager.getLength());
				// invokes calculateTime() function to get time in 00:00 format
				length.setText(videoManager.calculateTime((int) videoManager.getLength()));
			}
		});

		// Adding a video finished event listener
		videoManager.getComponent().getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			@Override
			public void finished(MediaPlayer mediaPlayer) {
				vidProgress.setValue(0);
				videoManager.setVisibility(false);
				btnPlay.setIcon(new ImageIcon(Main.class.getResource("/images/replay.png")));
				disableControlExclPlay(true);
			}
		});

		// Adding all components to their respective panels
		playbackPane.add(videoManager.getComponent(), BorderLayout.CENTER);
		playbackPane.add(progressPane, BorderLayout.SOUTH);
		progressPane.add(length, BorderLayout.EAST);
		progressPane.add(currentTime, BorderLayout.WEST);
		progressPane.add(vidProgress, BorderLayout.CENTER);

		timePane.add(timeField, BorderLayout.WEST);
		timePane.add(btnGo, BorderLayout.CENTER);

		playbackButtonPane.add(btnRW);
		playbackButtonPane.add(btnSkipBack);
		playbackButtonPane.add(btnPlay);
		playbackButtonPane.add(btnSkipForward);
		playbackButtonPane.add(btnFF);

		volumePane.add(volSlider);
		volumePane.add(btnMute);
		buttonsPane.add(timePane);
		buttonsPane.add(playbackButtonPane);
		buttonsPane.add(volumePane);
		videoPane.add(playbackPane, BorderLayout.CENTER);
		videoPane.add(buttonsPane, BorderLayout.SOUTH);
		
		listPane.addTab("AudioList", null, audioList, null);
		commentaryPane.addTab("Commentary", null, commentary, null);
		audioPane.addTab("Add Audio", null, audio, null);

		sideFeatures.add(commentaryPane, BorderLayout.CENTER);
		sideFeatures.add(audioPane, BorderLayout.SOUTH);
		
		sidePane.add(sideFeatures, BorderLayout.CENTER);
		sidePane.add(listPane, BorderLayout.EAST);
		
		contentPane.add(menuBar, BorderLayout.NORTH);
		contentPane.add(videoPane, BorderLayout.CENTER);
		contentPane.add(sidePane, BorderLayout.EAST);
		pack();

		// Playing video specified
		videoManager.playVideo(path);

	}

	// This method disables playback controls 
	public void disableControlExclPlay(boolean b) {
		if (b == true) {
			btnSkipBack.setEnabled(false);
			btnSkipForward.setEnabled(false);
			volSlider.setEnabled(false);
			vidProgress.setEnabled(false);
			btnMute.setEnabled(false);
			btnGo.setEnabled(false);
			btnFF.setEnabled(false);
			btnRW.setEnabled(false);
		} else {
			btnPlay.setEnabled(false);
			btnSkipBack.setEnabled(false);
			btnSkipForward.setEnabled(false);
			volSlider.setEnabled(false);
			vidProgress.setEnabled(false);
			btnMute.setEnabled(false);
			btnGo.setEnabled(false);
			btnFF.setEnabled(false);
			btnRW.setEnabled(false);
		}
	}

	// This method enables playback controls
	public void enableControl() {
		btnPlay.setEnabled(true);
		btnSkipBack.setEnabled(true);
		btnSkipForward.setEnabled(true);
		volSlider.setEnabled(true);
		vidProgress.setEnabled(true);
		btnMute.setEnabled(true);
		btnGo.setEnabled(true);
		btnFF.setEnabled(true);
		btnRW.setEnabled(true);

	}
}
