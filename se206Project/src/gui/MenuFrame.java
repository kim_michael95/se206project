package gui;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import main.Main;
import managers.MenuManager;

import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This class is the menu gui that opens up at launch.
 * It allows the user to select a video to launch the main frame or exit.
 * @author jkpop
 *
 */

@SuppressWarnings("serial")
public class MenuFrame extends JFrame {
	
	private JPanel contentPane;
	private MenuManager manager = new MenuManager(this);	
	
	public MenuFrame() {
		
		//Component declarations
		JLabel lblVidiVox = new JLabel();
		JButton btnExit = new JButton();
		JButton btnSelectVideo = new JButton();
		
		// Setting up the frame and content pane
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 100, 20, 100));
		contentPane.setLayout(new GridLayout(3,1,0,10));
		this.setContentPane(contentPane);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(700, 350);
		this.setTitle("Menu");
		this.setResizable(false);
		
		//Setting up the label
		lblVidiVox.setIcon(new ImageIcon(Main.class.getResource("/images/logoLite.png")));
		lblVidiVox.setPreferredSize(new Dimension(300, 80));
		
		//Setting up the exit button
		btnExit.setIcon(new ImageIcon(Main.class.getResource("/images/exit.png")));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnExit.setPreferredSize(new Dimension(200, 50));
		
		//Setting up the selectVideo button
		btnSelectVideo.setIcon(new ImageIcon(Main.class.getResource("/images/select.png")));
		btnSelectVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manager.btnSelectVideo();
			}
		});
		btnSelectVideo.setPreferredSize(new Dimension(200, 50));
		
		//Adding all components to the panel
		contentPane.add(lblVidiVox);
		contentPane.add(btnSelectVideo);
		contentPane.add(btnExit);
		pack();
	}
}
