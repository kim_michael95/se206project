package gui.subPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import tools.fileTools.AudioFile;
import tools.fileTools.FileOpener;

import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 * This class is the list gui which stores all the added audio tracks.
 * It calls the functionality to merge the audio to video via a swing worker.
 * And also has methods to add to list and remove from list.
 * @author jkpop
 *
 */
@SuppressWarnings("serial")
public class AudioListPane extends JPanel {

	
	// Declaring class fields and gui components
	private FileOpener videoOpener = new FileOpener(".avi");
	private JList<String> audioListComponent = new JList<String>();
	private ArrayList<AudioFile> audioList;
	private DefaultListModel<String> listModel = new DefaultListModel<String>();
	private String vidPath;

	public AudioListPane(ArrayList<AudioFile> list, String videoPath) {
		this.audioList = list;
		this.vidPath = videoPath;
		
		//Setting up the panel
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 0));
		this.setPreferredSize(new Dimension(170, 500));

		// Component declarations
		final JPanel buttonPane = new JPanel();
		JButton btnDelete = new JButton("Delete");
		JButton btnMerge = new JButton("Merge");
		JScrollPane scrollPane = new JScrollPane(audioListComponent);

		audioListComponent.setFont(new Font("Tahoma", Font.PLAIN, 12));

		// Setting up nested Panels
		buttonPane.setLayout(new GridLayout(1, 2));

		// Setting up the delete button
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = audioListComponent.getSelectedIndex();
				// Error checking to see if there is an audio track selected
				if (index < 0) {
					JOptionPane.showMessageDialog(buttonPane, "You must select an audio track", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					audioList.remove(index);
					listModel.remove(index);
				}
			}
		});

		// Setting up the merge button
		btnMerge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (audioList.size() > 0) {
					JOptionPane.showMessageDialog(buttonPane, "Select the name and location to save the merged file");
					String newVideoName = videoOpener.saveFile();
					if (newVideoName != null) {
						ProgressBar progressBar = new ProgressBar(vidPath, audioList, newVideoName);
						progressBar.setVisible(true);
					}
				} else {
					JOptionPane.showMessageDialog(buttonPane, "You must add atleast one audio track", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		// Adding components to respective panels
		buttonPane.add(btnDelete);
		buttonPane.add(btnMerge);

		this.add(scrollPane, BorderLayout.CENTER);
		this.add(buttonPane, BorderLayout.SOUTH);

	}

	// This method adds an audio file to the list
	public void addToList(AudioFile af) {
		listModel.addElement(af.toString());
		audioListComponent.setModel(listModel);
	}
	
	// This method clears the list of audio tracks
	public void clearList(){
		listModel.clear();
		audioList.clear();
		audioListComponent.setModel(listModel);
	}

}
