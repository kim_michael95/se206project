package gui.subPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import managers.VideoManager;
import tools.MaxCharacterLimiter;
import tools.fileTools.AudioFile;
import tools.fileTools.FileOpener;

@SuppressWarnings("serial")
public class AudioPane extends JPanel {

	// Declaring class fields needed
	private FileOpener fo = new FileOpener(".mp3");
	private File mp3;
	private ArrayList<AudioFile> audioList;
	private AudioListPane audioListPane;

	public AudioPane(final VideoManager videoManager, ArrayList<AudioFile> list,
			AudioListPane listPane) {
		// Setting up fields from constuctor
		this.audioListPane = listPane;
		this.audioList = list;
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 5));

		// Component Declarations
		// Declaring labels & Setting up labels
		JLabel lblMp3 = new JLabel("Type in a mp3's address or click \"Select\"");
		JLabel lblTime = new JLabel("Set Time:");
		JLabel lblVolume = new JLabel("Volume:");
		lblMp3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTime.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblVolume.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		// Declaring buttons
		JButton btnSelect = new JButton("Select");
		JButton btnCurrentTime = new JButton("Current Time");
		JButton btnAdd = new JButton("Add");

		// Declaring & Setting up text fields
		final JTextField timeField = new JTextField("00:00");
		timeField.setHorizontalAlignment(JTextField.CENTER);
		timeField.setDocument(new MaxCharacterLimiter(5));
		timeField.setText("00:00");
		timeField.setPreferredSize(new Dimension(50, 23));

		final JTextField mp3Field = new JTextField();
		mp3Field.setEditable(false);
		mp3Field.setPreferredSize(new Dimension(450, 23));

		// Declaring nested panels
		final JPanel mp3Pane = new JPanel();
		JPanel bottomPane = new JPanel();
		
		// Declaring slider
		final JSlider volSlider = new JSlider();

		// Setting up the volume slider
		volSlider.setValue(100);
		volSlider.setMaximum(200);
		volSlider.setMinimum(0);
		volSlider.setMinorTickSpacing(50);
		volSlider.setPaintTicks(true);
		volSlider.setSnapToTicks(true);
		volSlider.setFocusable(false);
		volSlider.setPreferredSize(new Dimension(100, 30));

		// Setting up the mp3 field
		mp3Field.setPreferredSize(new Dimension(200, 23));

		// Setting up current time button
		btnCurrentTime.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnCurrentTime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String time = videoManager.calculateTime((int) videoManager.getTime());
				if (time.length() == 4) {
					timeField.setText("0" + time);
				} else {
					timeField.setText(time);
				}
			}
		});
		btnCurrentTime.setPreferredSize(new Dimension(80, 28));

		// Setting up add button
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Checking if audio file is not selected
				if (mp3 != null) {
					// Setting up the added audio's volume from the slider
					double vol = 1.0;
					if (volSlider.getValue() == 50) {
						vol = 0.5;
					} else if (volSlider.getValue() == 100) {
						vol = 1.0;
					} else {
						vol = 1.5;
					}

					// Checking if the time selected is valid
					if (videoManager.isValidTime(timeField.getText()) == true) {
						//Generating audio file object
						AudioFile element = new AudioFile((int) videoManager.convertToLong(timeField.getText()),
								mp3.getAbsolutePath(), videoManager.getVideoPath(), vol);
						//Adding to the list
						audioList.add(element);
						audioListPane.addToList(element);
					}
				} else {
					JOptionPane.showMessageDialog(mp3Pane, "The audio file must be chosen", "Input Error",
							JOptionPane.ERROR_MESSAGE);

				}
			}
		});
		btnAdd.setPreferredSize(new Dimension(65, 23));

		// Setting up select button
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mp3 = fo.openFile();
				if (mp3 != null) {
					mp3Field.setText(mp3.getAbsolutePath());
				} else {
					mp3Field.setText("");
				}
			}
		});
		btnSelect.setPreferredSize(new Dimension(65, 23));

		// Adding components
		mp3Pane.add(mp3Field);
		mp3Pane.add(btnSelect);
		mp3Pane.add(btnAdd);

		bottomPane.add(lblTime);
		bottomPane.add(timeField);
		bottomPane.add(btnCurrentTime);
		bottomPane.add(lblVolume);
		bottomPane.add(volSlider);

		this.add(lblMp3, BorderLayout.NORTH);
		this.add(mp3Pane, BorderLayout.CENTER);
		this.add(bottomPane, BorderLayout.SOUTH);

	}

}
