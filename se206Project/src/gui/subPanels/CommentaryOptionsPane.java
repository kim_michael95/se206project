package gui.subPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import managers.VideoManager;
import tools.MaxCharacterLimiter;

/**
 *  This class is the commentary option panel, which is responsible
 *  for collecting data on what settings the user has set for the festival commentary.
 *  
 * @author jkpop
 *
 */
@SuppressWarnings("serial")
public class CommentaryOptionsPane extends JPanel {

	// Declaring comboboxes as class fields as methods need to return their value
	private final JComboBox<String> speedBox;
	private final JComboBox<String> emotionBox;
	private final JTextField timeField = new JTextField("00:00");
	
	public CommentaryOptionsPane(final VideoManager videoManager) {

		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 0));

		String[] speeds = { "Slow", "Normal", "Fast" };
		String[] emotions = { "Sad", "Normal", "Happy" };

		// Component Declaration
		// Decalring nested panels
		JPanel festivalPane = new JPanel();
		JPanel timePane = new JPanel();
		JPanel festivalHolder = new JPanel();
		JPanel timeHolder = new JPanel();
		JPanel timeAndSeparator = new JPanel();
		JPanel deafaultAndSeparator = new JPanel();
		JPanel headerAndSeparator = new JPanel();
		
		// Declaring & setting up jlabels
		JLabel lblHeader = new JLabel("Commentary Options");
		JLabel lblSpeed = new JLabel("Speed:");
		lblSpeed.setFont(new Font("Tahoma", Font.PLAIN, 12));
		JLabel lblEmotion = new JLabel("Emotion:");
		lblEmotion.setFont(new Font("Tahoma", Font.PLAIN, 12));
		JLabel lblTime = new JLabel("Set Time:");
		lblTime.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		// Declaring buttons
		JButton btnDefault = new JButton("Default");
		JButton btnCurrentTime = new JButton("Current Time");
		
		//Setting up nested panels
		timeAndSeparator.setLayout(new BorderLayout());
		headerAndSeparator.setLayout(new BorderLayout());
		deafaultAndSeparator.setLayout(new BorderLayout());
		festivalPane.setLayout(new GridLayout(2, 2, 0, 2));
		timePane.setLayout(new GridLayout(2, 2, 0, 2));
		
		// Initializing and setting up comboboxes 
		speedBox = new JComboBox<String>(speeds);
		speedBox.setPreferredSize(new Dimension(100, 23));
		speedBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		speedBox.setSelectedIndex(1);
		emotionBox = new JComboBox<String>(emotions);
		emotionBox.setPreferredSize(new Dimension(100, 23));
		emotionBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		emotionBox.setSelectedIndex(1);

		// Setting up time field
		timeField.setDocument(new MaxCharacterLimiter(5));
		timeField.setHorizontalAlignment(JTextField.CENTER);
		timeField.setText("00:00");


		// Setting up the default button
		btnDefault.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				speedBox.setSelectedIndex(1);
				emotionBox.setSelectedIndex(1);
				timeField.setText("00:00");
			}
		});
		btnDefault.setPreferredSize(new Dimension(70, 20));

		// Setting up the current time button
		btnCurrentTime.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnCurrentTime.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String time = videoManager.calculateTime((int) videoManager.getTime());
				if (time.length() == 4) {
					timeField.setText("0" + time);
				} else {
					timeField.setText(time);
				}
			}
		});
		btnCurrentTime.setPreferredSize(new Dimension(80, 23));

		// Adding components to respective panels
		festivalPane.add(lblSpeed);
		festivalPane.add(speedBox);
		festivalPane.add(lblEmotion);
		festivalPane.add(emotionBox);

		timePane.add(lblTime);
		timePane.add(timeField);
		timePane.add(new JLabel());
		timePane.add(btnCurrentTime);

		timeAndSeparator.add(festivalHolder, BorderLayout.WEST);
		timeAndSeparator.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.CENTER);
		deafaultAndSeparator.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.NORTH);
		deafaultAndSeparator.add(btnDefault, BorderLayout.EAST);
		headerAndSeparator.add(lblHeader, BorderLayout.NORTH);
		headerAndSeparator.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.CENTER);
		festivalHolder.add(festivalPane);
		timeHolder.add(timePane);

		this.add(timeAndSeparator, BorderLayout.WEST);
		this.add(timeHolder, BorderLayout.CENTER);
		this.add(deafaultAndSeparator, BorderLayout.SOUTH);
		this.add(headerAndSeparator, BorderLayout.NORTH);
	}
	
	//This method returns speed from speed combobox
	public int getSpeed(){
		return speedBox.getSelectedIndex() + 1;
	}
	
	//This method returns emotion from emotion combobox
	public int getEmotion(){
		return emotionBox.getSelectedIndex() + 1;
	}
	
	//This method returns the time from the time field
	public String getTime(){
		return timeField.getText();
	}
	

}
