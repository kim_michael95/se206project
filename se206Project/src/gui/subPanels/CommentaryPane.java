package gui.subPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import gui.MainFrame;
import managers.VideoManager;
import tools.MaxCharacterLimiter;
import tools.festivalTools.FestivalScheme;
import tools.fileTools.FileOpener;
import tools.swingworkers.CreateMP3;
import tools.swingworkers.FestivalWorker;

/**
 * This class is the commentary panel. It is responsible for reading the commentary eneterd by the user.
 * It calls a swing worker to play the commentary or cancel it. 
 * It also calls a swing worker to generate an mp3 file with the commentary in it using the specified options from the CommentaryOptionsPane.
 * It generates a scheme file object and generates a file using the methods from FestivalScheme class.
 * It also calls a swing worker to add the audio to the current file. It does this by generating a new file with VidiVox in the name and opening,
 * the new video.
 * @author jkpop
 *
 */
@SuppressWarnings("serial")
public class CommentaryPane extends JPanel {

	// Declaring class fields
	private JPanel contentPane;
	private String commentary;
	private FileOpener fo = new FileOpener(".mp3");
	private CreateMP3 mp3Task;
	private FestivalWorker festivalWorker;

	public CommentaryPane(final VideoManager videoManager, final AudioListPane list, final MainFrame mf) {
		// Setting up panel
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 0));

		// Declaring components
		final JTextArea textArea = new JTextArea(5, 20);
		JLabel lblCharacterLimit = new JLabel("Enter commentary text (160 char limit)");
		final JPanel buttonPane = new JPanel();
		JPanel headerPane = new JPanel();
		JPanel centerPane = new JPanel();
		JScrollPane scrollPane = new JScrollPane(textArea);
		final JButton btnHear = new JButton("Hear");
		JButton btnAddNow = new JButton("Add voice");
		JButton btnSave = new JButton("Save mp3");
		final CommentaryOptionsPane options = new CommentaryOptionsPane(videoManager);
		final FestivalScheme festivalScheme = new FestivalScheme(2, 2, videoManager);

		// Setting up nestedPanels
		headerPane.setLayout(new BorderLayout());
		centerPane.setLayout(new BorderLayout());

		// Setting up the text limit label
		lblCharacterLimit.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCharacterLimit.setHorizontalAlignment(SwingConstants.LEFT);

		// Setting up the textArea
		textArea.setColumns(10);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setDocument(new MaxCharacterLimiter(160)); // adding text limit

		// Setting up the hear button
		btnHear.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnHear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commentary = textArea.getText();
				if (btnHear.getText().equals("Hear")) {
					if (commentary.isEmpty() || commentary.trim().length() == 0) {
						JOptionPane.showMessageDialog(buttonPane, "Text can not be blank", "Text Error",
								JOptionPane.ERROR_MESSAGE);
					} else {
						btnHear.setText("Stop");
						commentary = commentary.replace("\n", "");
						festivalScheme.setScheme(options.getSpeed(), options.getEmotion());
						festivalWorker = new FestivalWorker(commentary, btnHear, festivalScheme);
						festivalWorker.execute();
					}
				} else {
					btnHear.setText("Hear");
					festivalWorker.cancel(true);

				}
			}
		});
		btnHear.setPreferredSize(new Dimension(80, 23));

		// Setting up the createMP3 button
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commentary = textArea.getText();
				if (commentary.isEmpty() || commentary.trim().length() == 0) {
					JOptionPane.showMessageDialog(buttonPane, "Text can not be blank", "Text Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					String mp3 = fo.saveFile();
					if (mp3 != null) {
						mp3 = mp3.substring(0, mp3.length() - 4);

						commentary = commentary.replace("\n", "");
						festivalScheme.setScheme(options.getSpeed(), options.getEmotion());
						// Creates the mp3 file in the background
						mp3Task = new CreateMP3(commentary, mp3, contentPane, festivalScheme);
						mp3Task.execute();
					}
				}
			}
		});
		btnSave.setPreferredSize(new Dimension(80, 23));

		// Setting up the addNow button
		btnAddNow.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnAddNow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commentary = textArea.getText();
				// Checking if commentary is blank
				if (commentary.isEmpty() || commentary.trim().length() == 0) {
					JOptionPane.showMessageDialog(buttonPane, "Text can not be blank", "Text Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					list.clearList();
					commentary = commentary.replace("\n", "");
					String time = options.getTime();
					int delay = (int) videoManager.convertToLong(time);
					festivalScheme.setScheme(options.getSpeed(), options.getEmotion());
					// Creates the mp3 file in the background
					ProgressBar progressBar = new ProgressBar(festivalScheme, commentary, videoManager, delay);
					progressBar.setVisible(true);
					mf.enableControl();
				}
			}
		});
		btnAddNow.setPreferredSize(new Dimension(80, 23));

		// Adding components to panels
		buttonPane.add(btnHear);
		buttonPane.add(btnSave);
		buttonPane.add(btnAddNow);

		headerPane.add(lblCharacterLimit, BorderLayout.NORTH);
		headerPane.add(scrollPane, BorderLayout.CENTER);

		centerPane.add(buttonPane, BorderLayout.NORTH);
		centerPane.add(options, BorderLayout.CENTER);

		this.add(headerPane, BorderLayout.NORTH);
		this.add(centerPane, BorderLayout.CENTER);

	}

}
