package gui.subPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import managers.VideoManager;
import tools.festivalTools.FestivalScheme;
import tools.fileTools.AudioFile;
import tools.swingworkers.AddAndPlay;
import tools.swingworkers.MergeWorker;

/**
 * This class is the progress bar panel which displays the progress bar during swing worker actions.
 * @author jkpop
 *
 */
@SuppressWarnings("serial")
public class ProgressBar extends JFrame {
	// Declaring class fields
	private JPanel contentPane;
	private JProgressBar progressBar;
	private JLabel label = new JLabel();
	private MergeWorker mergeTask;
	private AddAndPlay addNowTask;
	
	public ProgressBar(String vidPath,ArrayList<AudioFile> audioList, String newVideoName) {
		// Setting up the contentPanel
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(5,5));
		this.setContentPane(contentPane);
		this.setTitle("Processing...");
		this.setPreferredSize(new Dimension(500,60));
		this.setResizable(false);
			
		// Adding components to the content pane
		progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		contentPane.add(progressBar, BorderLayout.SOUTH);
		
		label.setText("Adding the audio files to the video...");
		contentPane.add(label, BorderLayout.NORTH);
		pack();	
		
		// Creating the new video file in the background
		mergeTask = new MergeWorker(audioList, contentPane, newVideoName, this, progressBar);
		mergeTask.execute();	
	}
	
	public ProgressBar(FestivalScheme festivalScheme, String commentary, VideoManager videoManager, int time) {
		// Setting up the contentPanel
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(5,5));
		this.setContentPane(contentPane);
		this.setTitle("Processing...");
		this.setPreferredSize(new Dimension(500,60));
		this.setResizable(false);
			
		// Adding the progress bar
		progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		contentPane.add(progressBar, BorderLayout.SOUTH);
		
		label.setText("Adding the commentary...");
		contentPane.add(label, BorderLayout.NORTH);
		pack();	
		
		// Creating the new video file in the background
		addNowTask = new AddAndPlay(contentPane, festivalScheme, commentary, videoManager, time, progressBar, this);
		addNowTask.execute();	
	}
}
