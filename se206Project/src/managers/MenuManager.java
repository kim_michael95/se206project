package managers;

import java.io.File;
import javax.swing.JFrame;
import gui.MainFrame;
import tools.fileTools.FileOpener;

public class MenuManager {
	
	private JFrame frame;
	private FileOpener fo;

	// Constructor for manager
	public MenuManager(JFrame frame){
		this.frame = frame;
		this.fo = new FileOpener(".avi", frame);
	}
	
	// This method deals with the btnSelectVideo functionality of the frame
	public void btnSelectVideo(){
		File vid = fo.openFile();
		if (vid != null){
			frame.setVisible(false);
			try {
				MainFrame vp;
				vp = new MainFrame(vid.getAbsolutePath());
				vp.setVisible(true);
			} catch (Exception e1) {
				e1.printStackTrace();
			}		
		}
		
	}

}
