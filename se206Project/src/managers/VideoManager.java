package managers;

import java.awt.Dimension;
import javax.swing.JOptionPane;

import tools.swingworkers.FastForward;
import tools.swingworkers.Rewind;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;

public class VideoManager {
	
	// Declaring class fields
	// Video components 
	private EmbeddedMediaPlayerComponent mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
	private EmbeddedMediaPlayer video = mediaPlayerComponent.getMediaPlayer();
	// Swing workers
	private FastForward ffWorker;
	private Rewind rwWorker;
	
	private boolean isMuted;
	private String videoPath;

	// Constructor
	public VideoManager() {
		mediaPlayerComponent.setPreferredSize(new Dimension(600,338));
	};

	// The following methods deal with video play back and volume functionality
	public void playVideo(String path) {
		video.startMedia(path);
	}

	public void pauseVideo(boolean b) {
		video.setPause(b);
	}

	public void closeVideo() {
		if (video != null) {
			mediaPlayerComponent.setVisible(false);
			video.stop();
		}
	}

	public void skipForward() {
		video.skip(5000);
	}

	public void skipBack() {
		video.skip(-5000);
	}

	public void rewind(boolean b) {
		if (b == true) {
			isMuted = video.isMute();
			rwWorker = new Rewind(video);
			video.setPause(true);
			rwWorker.execute();
			video.mute(true);
		} else {
			video.setPause(false);
			rwWorker.cancel(true);
			video.mute(isMuted);
		}
	}

	public void fastForward(boolean b) {
		if (b == true) {
			isMuted = video.isMute();
			ffWorker = new FastForward(video);
			video.setPause(true);
			ffWorker.execute();
			video.mute(true);
		} else {
			video.setPause(false);
			ffWorker.cancel(true);
			video.mute(isMuted);
		}
	}
	
	public void changeVideo(String path){
		mediaPlayerComponent.setVisible(true);
		this.videoPath = path;
		this.playVideo(path);
	}
	
	public void muteToggle() {
		if (video.isMute() == false) {
			video.mute(true);
		} else {
			video.mute(false);
		}
	}

	public void mute(boolean b) {
		video.mute(b);
	}

	public void setVolume(int volume) {
		video.setVolume(volume);
	}

	public void setVisibility(boolean b) {
		mediaPlayerComponent.setVisible(b);
	}

	public void setTime(long time) {
		video.setTime(time);
	}
	
	public void setVideoPath(String s){
		this.videoPath = s;
	}

	public void setSize(Dimension size) {
		mediaPlayerComponent.setPreferredSize(size);
	}

	public void goTime(String s) {
		if (this.isValidTime(s) == true) {
			video.setTime(convertToLong(s));
		}
	}

	// The following methods return values from the videos status
	public boolean getVisibility() {
		return mediaPlayerComponent.isVisible();
	}

	public boolean isPlaying() {
		return video.isPlaying();
	}

	public long getTime() {
		return video.getTime();
	}

	public int getVolume() {
		return video.getVolume();
	}

	public long getLength() {
		return video.getLength();
	}

	public String getVideoPath() {
		return videoPath;
	}

	public boolean isMute() {
		return video.isMute();
	}

	public EmbeddedMediaPlayerComponent getComponent() {
		return mediaPlayerComponent;
	}
	
	// This is a utility method which determines if a string is a valid time stamp or not
	public boolean isValidTime(String s) {
		if (s.isEmpty() || s.trim().length() == 0) {
			JOptionPane.showMessageDialog(mediaPlayerComponent,
					"Invalid Time Format: The time field must not be empty.\nThe time must be in 00:00 (mm:ss) format with leading 0's.",
					"Time Error", JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (s.length() != 5) {
			JOptionPane.showMessageDialog(mediaPlayerComponent,
					"Invalid Time Format: The time must be in 00:00 (mm:ss) format with leading 0's.", "Time Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (s.charAt(2) != ':') {
			JOptionPane.showMessageDialog(mediaPlayerComponent,
					"Invalid Time Format: the time must be in 00:00 (mm:ss) format with leading 0's.", "Time Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if ((s.substring(0, 2) + s.substring(3, 5)).matches("[0-9]+") == false) {
			JOptionPane.showMessageDialog(mediaPlayerComponent, "Invalid Character: Time contains invalid characters.",
					"Time Error", JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (video.getLength() <= convertToLong(s)) {
			JOptionPane.showMessageDialog(mediaPlayerComponent,
					"Invalid Time Value: The time must be smaller than the length of the video.", "Time Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else {
			return true;
		}

	}

	// Calculates the time stamp for the video converts int milliseconds into
	// 00:00 string format
	public String calculateTime(int time) {
		int lengthS = time / 1000;
		int lengthM = lengthS / 60;
		lengthS = lengthS - lengthM * 60;
		if (lengthS < 10) {
			return lengthM + ":" + "0" + lengthS;
		} else {
			return lengthM + ":" + lengthS;
		}
	}

	// Calculates the time from 00:00 format
	public long convertToLong(String s) {
		String[] parts = s.split(":");
		int mins = Integer.parseInt(parts[0]);
		int secs = Integer.parseInt(parts[1]);
		return (mins * 60000) + (secs * 1000);
	}
}
