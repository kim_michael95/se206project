package tools;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * This class extends PlainDocument and sets the limit of characters a text field/ area can have.
 * @author jkpop
 *
 */
@SuppressWarnings("serial")
public class MaxCharacterLimiter extends PlainDocument {
	private int limit;
	
	// Constructor
	public MaxCharacterLimiter(int limit) {
		this.limit = limit;
	}
	
	// Creates the text character limit
	public void insertString(int offset, String str, AttributeSet set) throws BadLocationException {
		if (str.isEmpty() || (str == null)) {
			return;
		} else if ((getLength() + str.length() <= limit)) {
			str.toUpperCase();
			super.insertString(offset, str, set);
		}
	}
}
