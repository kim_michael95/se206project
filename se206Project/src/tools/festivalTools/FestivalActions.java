package tools.festivalTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;

/**
 * This class contains methods to get pid and kill the pid specified. It kills the festival command in otherwords.
 * @author jkpop
 *
 */
public class FestivalActions {
	public FestivalActions(){
	};
	
	
	// This method gets the pid(Process id) of the process given as a parameter
	private String getPid(Process p) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
		if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
			Field f = p.getClass().getDeclaredField("pid");
			f.setAccessible(true);
			return Integer.toString(f.getInt(p));
		} else {
			return "not a process";
		}
	}
	
	// This method uses the getPid method to get the pid and kill it if the process is not null.
	public void killProcess(Process p) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
		if (p == null){
			return;
		}
		String pid = getPid(p);
		boolean stop = false;
		String killID = "";
		ProcessBuilder builder = new ProcessBuilder("bash", "-c", "pstree -pl " + pid);
		Process psTree;
		try {
			psTree = builder.start();
			InputStream stdout = psTree.getInputStream();
			BufferedReader stdoutBuffered = new BufferedReader(new InputStreamReader(stdout));
			String result = stdoutBuffered.readLine();
			if (result != null && !result.equals("")) {
				int i = result.indexOf("play");
				String substr = result.substring(i);
				int start = substr.indexOf("(");
				int end = substr.indexOf(")");
				killID = substr.substring(start + 1, end);
			} else {
				stop = true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (stop == false) {
			ProcessBuilder killBuilder = new ProcessBuilder("bash", "-c", "kill -9 " + killID);
			try {
				killBuilder.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
