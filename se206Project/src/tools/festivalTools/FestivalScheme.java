package tools.festivalTools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import managers.VideoManager;

/**
 * This class is the festival scheme class which is intended to be instantiated.
 * It holds the festival command parameters such as the rate of speech and pitch values.
 * It also contains methods to generate a txt file with the commentary and also an scm file which is used
 * for the festival command.
 * @author jkpop
 *
 */
public class FestivalScheme {

	// Class fields
	private File scm;
	private File txt;
	private VideoManager videoManager;
	private String scmName;
	private String txtName;
	private double schemeSpeed;
	private int maxPitch;
	private int minPitch;
	private FileWriter writer;

	// Constructor
	public FestivalScheme(int speed, int emotion, VideoManager videoManager) {
		this.videoManager = videoManager;
		this.setScheme(speed, emotion);
	}

	// Setting the scheme
	public void setScheme(int speed, int emotion) {
		if (speed == 1) {
			schemeSpeed = 2.2;
		} else if (speed == 2) {
			schemeSpeed = 1.0;
		} else {
			schemeSpeed = 0.8;
		}

		if (emotion == 1) {
			maxPitch = 90;
			minPitch = 85;
		} else if (emotion == 2) {
			maxPitch = 120;
			minPitch = 105;
		} else {
			maxPitch = 140;
			minPitch = 110;
		}

	}

	// Generating the scm
	public void generateSCM(boolean isTalking, String commentary) {
		scmName = videoManager.getVideoPath().substring(0, videoManager.getVideoPath().length() - 4) + "kkim382.scm";
		scm = new File(scmName);

		try {
			writer = new FileWriter(scm);
			BufferedWriter bw = new BufferedWriter(writer);
			bw.write("(set! duffint_params '((start " + maxPitch + ") (end " + minPitch + ")))\n");
			bw.write("(Parameter.set 'Int_Method 'DuffInt)\n");
			bw.write("(Parameter.set 'Int_Target_Method Int_Targets_Default)\n");
			bw.write("(Parameter.set 'Duration_Stretch " + schemeSpeed + ")");
			if (isTalking == true){
				bw.write("(SayText \""+ commentary + "\")");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Deleting the scm
	public void deleteSCM() {
		File file = new File(scmName);
		file.delete();
	}
	
	// Generating the txt file
	public void generateTxt(String commentary) {
		txtName = videoManager.getVideoPath().substring(0, videoManager.getVideoPath().length() - 4) + "kkim382.txt";
		txt = new File(txtName);
		try {
			writer = new FileWriter(txt);
			BufferedWriter bw = new BufferedWriter(writer);
			bw.write(commentary);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Deleting the txt file
	public void deleteTXT() {
		File file = new File(txtName);
		file.delete();
	}
	
	// The following methods return the files absolute paths
	public String getSCM(){
		return scmName;
	}
	
	public String getTXT(){
		return txtName;
	}

}
