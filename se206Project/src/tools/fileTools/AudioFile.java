package tools.fileTools;

/**
 * This class is the Audio File class, it containts the parameters required of the audio file to be added to the video.
 * 
 * @author jkpop
 *
 */
public class AudioFile {

	// Class fields
	private int time;
	private String path;
	private String vidPath;
	private double volume;

	// Constructore
	public AudioFile(int time, String path, String vidPath, double volume) {
		this.time = time;
		this.path = path;
		this.vidPath = vidPath;
		this.volume = volume;
	}

	// This method returns the festival command which adds the audio to the video excluding the name of the output file.
	public String getCommand() {
		// Checks whether it needs delalay or not and returns the correct command
		if (time > 0) {
			return "ffmpeg -y -i " + vidPath + " -i " + path + " -filter_complex \"[1:a]volume=" + volume
					+ "[aud2];[aud2]adelay=" + time + "[aud3];[aud3][0:a]amix=inputs=2\" ";
		} else {
			return "ffmpeg -y -i " + vidPath + " -i " + path + " -filter_complex \"[1:a]volume=" + volume
					+ "[aud2];[aud2][0:a]amix=inputs=2\" ";
		}
	}

	// gets the name of the file
	public String getName() {
		int startIndex = 0;
		for (int i = 0; i < path.length(); i++) {
			if (path.charAt(i) == '/') {
				startIndex = i + 1;
			}
		}
		return path.substring(startIndex);
	}

	// gets the path of the video to add to
	public void setVidPath(String s){
		this.vidPath = s;
	}
	
	// gets the time of the of the video that the audio will add to
	public String getTime() {
		int lengthS = time / 1000;
		int lengthM = lengthS / 60;
		lengthS = lengthS - lengthM * 60;
		if (lengthS < 10) {
			return lengthM + ":" + "0" + lengthS;
		} else {
			return lengthM + ":" + lengthS;
		}
	}

	@Override
	public String toString() {
		return this.getName() + " - " + this.getTime();
	}
}
