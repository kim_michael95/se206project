package tools.fileTools;

import java.io.File;

import javax.swing.filechooser.FileFilter;

// This class is a file filter for the JFileChooser when choosing files.
// Depending on the type required it has different filters.
public class FileFilters extends FileFilter {

	private TYPES type;

	public FileFilters(TYPES type) {
		this.type = type;
	}

	@Override
	public boolean accept(File f) {
		if (type.equals(TYPES.AVI)) {
			if (f.isDirectory()) {
				return true;
			} else {
				return f.getName().toLowerCase().endsWith(".avi");
			}
		} else if (type.equals(TYPES.MP3)){
			if (f.isDirectory()) {
				return true;
			} else {
				return f.getName().toLowerCase().endsWith(".mp3");
			}
		} else {
			return false;
		}
	}

	@Override
	public String getDescription() {
		if (type.equals(TYPES.AVI)) {
			return "AVI files (*.avi)";
		} else if (type.equals(TYPES.MP3)) {
			return "MP3 files (*.mp3)";
		} else {
			return null;
		}
	}

}
