package tools.swingworkers;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import managers.VideoManager;
import tools.festivalTools.FestivalScheme;
import tools.fileTools.AudioFile;

/**
 * This swing worker is responsible for adding the audio to the current video
 * and playing it. It takes the video and audio from the files and outputs it to
 * a file with the original file name with an extension and plays it.
 * 
 * @author jkpop
 *
 */
public class AddAndPlay extends SwingWorker<Void, Void> {

	// Class fields
	private JFrame parentFrame;
	private JPanel parentPanel;
	private FestivalScheme festivalScheme;
	private String commentary;
	private VideoManager videoManager;
	private String output;
	private AudioFile audio;
	private int time;
	private JProgressBar progressBar;

	// Constructor
	public AddAndPlay(JPanel parentPanel, FestivalScheme festivalScheme, String commentary, VideoManager videoManager,
			int time, JProgressBar progressBar, JFrame parentFrame) {
		this.parentPanel = parentPanel;
		this.festivalScheme = festivalScheme;
		this.commentary = commentary;
		this.videoManager = videoManager;
		this.time = time;
		this.progressBar = progressBar;
		this.parentFrame = parentFrame;
	}

	@Override
	public Void doInBackground() {

		// Performs the previous commands in bash
		try {
			festivalScheme.generateSCM(false, commentary);
			festivalScheme.generateTxt(commentary);
			String mp3Name = festivalScheme.getSCM().substring(0, festivalScheme.getSCM().length() - 4) + "_temp";

			String cmd = "text2wave -o " + mp3Name + ".wav " + festivalScheme.getTXT() + " -eval "
					+ festivalScheme.getSCM();
			String cmd2 = "ffmpeg -i " + mp3Name + ".wav" + " -f mp3 " + mp3Name + ".mp3";
			String cmd3 = "rm " + mp3Name + ".wav";

			ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", cmd);
			ProcessBuilder pb2 = new ProcessBuilder("/bin/bash", "-c", cmd2);
			ProcessBuilder pb3 = new ProcessBuilder("/bin/bash", "-c", cmd3);

			Process p = pb.start();
			p.waitFor();
			Process p2 = pb2.start();
			p2.waitFor();
			Process p3 = pb3.start();
			p3.waitFor();

			audio = new AudioFile(time, mp3Name + ".mp3", videoManager.getVideoPath(), 1.0);

			output = festivalScheme.getSCM().substring(0, festivalScheme.getSCM().length() - 4) + "VidiVox.avi";
			ProcessBuilder mergeBuilder = new ProcessBuilder("/bin/bash", "-c", audio.getCommand() + output);
			Process merge = mergeBuilder.start();
			merge.waitFor();

			String cmd4 = "rm " + mp3Name + ".mp3";
			ProcessBuilder pb4 = new ProcessBuilder("/bin/bash", "-c", cmd4);
			Process p4 = pb4.start();
			p4.waitFor();

		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	// Ends the progress bar and shows successful message to user
	@Override
	protected void done() {
		videoManager.changeVideo(output);
		festivalScheme.deleteSCM();
		festivalScheme.deleteTXT();
		parentFrame.dispose();
		progressBar.setIndeterminate(false);
		JOptionPane.showMessageDialog(parentPanel, "Successfully saved new video in " + output);
	}
}
