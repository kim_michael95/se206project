package tools.swingworkers;

import java.io.IOException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.SwingWorker;

import tools.festivalTools.FestivalActions;
import tools.festivalTools.FestivalScheme;

public class FestivalWorker extends SwingWorker<Void, Void> {

	private String commentary;
	private Process p;
	private JButton button;
	private FestivalActions festivalActions = new FestivalActions();
	private FestivalScheme festivalScheme;

	// Constructor
	public FestivalWorker(String commentary, JButton button, FestivalScheme festivalScheme) {
		this.commentary = commentary;
		this.button = button;
		this.festivalScheme = festivalScheme;
	}

	public Process getProcess() {
		return p;
	}

	@Override
	protected Void doInBackground() throws Exception {
		try {
			if (!isCancelled()) {
				festivalScheme.generateSCM(true, commentary);
				String cmd = "festival -b " + festivalScheme.getSCM();
				ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", cmd);
				p = pb.start();
				p.waitFor();
			} else {
				p.destroy();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	@Override
	protected void done() {
		try {
			get();
			button.setText("Hear");
			festivalScheme.deleteSCM();
		} catch (InterruptedException | ExecutionException | CancellationException e) {
			try {
				festivalActions.killProcess(p);
				button.setText("Hear");
				festivalScheme.deleteSCM();
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
				e1.printStackTrace();
			}
		}
	};
}
