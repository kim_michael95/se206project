package tools.swingworkers;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import gui.subPanels.ProgressBar;
import tools.fileTools.AudioFile;

public class MergeWorker extends SwingWorker<Void, Void> {

	private JPanel parentPanel;
	private String newVideoName;
	private ProgressBar parentFrame;
	private JProgressBar progressBar;
	private ArrayList<AudioFile> audioList = new ArrayList<AudioFile>();

	// Constructor
	public MergeWorker(ArrayList<AudioFile> arrayList, JPanel parentPanel, String newVideoName,
			ProgressBar parentFrame, JProgressBar progressBar) {
		this.parentPanel = parentPanel;
		this.newVideoName = newVideoName;
		this.parentFrame = parentFrame;
		this.progressBar = progressBar;
		this.audioList = arrayList;
	}

	@Override
	public Void doInBackground() {

		// Performs the previous commands in bash
		try {
			String tempName = newVideoName.substring(0, newVideoName.length() - 4) + "0.avi";
			ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", audioList.get(0).getCommand() + tempName);
			Process p = pb.start();
			p.waitFor();
			if (audioList.size() > 1) {
				for (int i = 1; i < audioList.size(); i++) {
					audioList.get(i).setVidPath(tempName);
					pb = new ProcessBuilder("/bin/bash", "-c", audioList.get(i).getCommand()
							+ newVideoName.substring(0, newVideoName.length() - 4) + i + ".avi");
					p = pb.start();
					p.waitFor();
					pb = new ProcessBuilder("/bin/bash", "-c", "rm -f "+ tempName);
					p = pb.start();
					p.waitFor();
					tempName = newVideoName.substring(0, newVideoName.length() - 4) + i + ".avi";
				}
				pb = new ProcessBuilder("/bin/bash", "-c", "mv "+ tempName + " " + newVideoName);
				p = pb.start();
				p.waitFor();
			} else {
				pb = new ProcessBuilder("/bin/bash", "-c", "mv " + tempName + " " + newVideoName);
				p = pb.start();
				p.waitFor();
			}
		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	// Ends the progress bar and shows successful message to user
	@Override
	protected void done() {
		parentFrame.dispose();
		progressBar.setIndeterminate(false);
		JOptionPane.showMessageDialog(parentPanel, "Successfully saved new video in " + newVideoName);
	}
}
